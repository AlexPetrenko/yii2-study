<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="login-box">
	<div class="login-logo">
		<a href="../../index2.html"><b>Admin</b>LTE</a>
	</div><!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">Sign in to start your session</p>
			<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

				<?= $form->field($model, 'username', ['options' => [
					'tag' => 'div',
					'class' => 'form-group field-loginform-username has-feedback required',
					],
					'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}{hint}'

				])->textInput(); ?>

				<?= $form->field($model, 'password', ['options' => [
					'tag' => 'div',
					'class' => 'form-group field-loginform-password has-feedback required',
					],
				  	'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}{hint}'

				])->passwordInput() ?>

				<?= $form->field($model, 'rememberMe', [
					'options' => [
						'class' => 'checkbox icheck'
					],
					'template' => ' <div class="row"><div class="col-xs-8">{input}</div></div>'
				])->checkbox() ?>

				<div class="form-group">
					<?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
				</div>

			<?php ActiveForm::end(); ?>
	</div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php
$script  = <<< JS

    $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });

JS;
$this->registerJs($script);
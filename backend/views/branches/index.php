<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Branches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branches-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Create Branches', [
			'value' => \yii\helpers\Url::to('index.php?r=branches/create'),
			'class' => 'btn btn-success',
			'id'  => 'modalButton'
		]) ?>
    </p>
	<?php
		Modal::begin([
			'header' => '<h4>Create branch</h4>',
			'id' => 'modal',
			'size' => 'modal-lg'
		]);

		echo '<div id="modalContent"></div>';

		Modal::end();
	?>
	<?php Pjax::begin(['id' => 'branchesGrid']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'export' =>false,
		'pjax' => true,
		'rowOptions' => function($model) {
			if ($model->branch_status== 'inactive') {
				return ['class' => 'danger'];
			}
			else if ($model->branch_status== 'active') {
				return ['class' => 'success'];
			}
		},
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'companies_company_id',
				'value' => 'companiesCompany.company_name',
			],
			[
				'class' => 'kartik\grid\EditableColumn',
				'header' => 'BRANCH',
				'attribute' => 'branch_name'
			],
            'companiesCompany.company_name',
            'branch_address',
            'branch_created_date',
            // 'branch_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

	<?php Pjax::end(); ?>
</div>


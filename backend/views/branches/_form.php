<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \backend\models\Companies;
use \yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Branches */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branches-form">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

    <?= $form->field($model, 'companies_company_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Companies::find()->all(), 'company_id', 'company_name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<< JS

	$('form#{$model->formName()}').on('beforeSubmit', function(e){

		var form = $(this);
		$.post(
			form.attr('action'),
			form.serialize()
		)
			.done(function(result){ console.log(result);
				if(parseInt(result) === 1) { console.log(result);
					$(form).trigger('reset');
					//$(document).find('#modal').modal('hide');
					$.pjax.reload({container : '#branchesGrid'});
				}
				else {
					$(form).trigger('reset');
					$('#message').html(result.message);
				}
			})
			.fail(function(){
				console.log('server error');
			});
			return false;
	});

JS;
$this->registerJs($js);
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\models\Locations;

/* @var $this yii\web\View */
/* @var $model backend\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip_code')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Locations::find()->all(), 'location_id', 'zip_code'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a zip-code ...', 'id' => 'zipCode'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$url = \yii\helpers\Url::to('@web/index.php/locations/get-city-province');
$script  = <<< JS

    $('#zipCode').on('change', function(){

        $.get('$url',
        {
            id : $(this).val()
        },
         function(data){
            data = $.parseJSON(data);
            $('#customers-city').val(data.city);
            $('#customers-province').val(data.province);
         });
    });

JS;
$this->registerJs($script);
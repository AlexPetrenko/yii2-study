<?php

namespace backend\controllers;

use Yii;
use backend\models\Branches;
use backend\models\BranchesSearch;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BranchesController implements the CRUD actions for Branches model.
 */
class BranchesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Branches models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BranchesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


		if (Yii::$app->request->post('hasEditable')) {
				$branchId = Yii::$app->request->post('editableKey');
			$branch = Branches::findOne($branchId);

			$out = Json::encode(['output' => '', 'message' => '']);
			$post = [];
			$posted = current($_POST['Branches']);
			$post['Branches'] = $posted;
			if ($branch->load($post)) {
				$branch->save();
				$out = Json::encode(['output' => 'changed ' . $branch->branch_name, 'message' => '']);
			}
			echo $out;
			return;
		}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionImportExcel()
	{
		$inputFile = 'uploads/branches/import_php_excel.xls';
		$successMessage = '';
		$errorMessage = '';

		try {
			$inputFileType = \PHPExcel_IOFactory::identify($inputFile);
			$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFile);
		}
		catch(Exception $e) {
		//	die('Error');
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		for ($row = 1; $row <= $highestRow; $row += 1) {
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

			if ($row == 1) {
				continue;
			}


			$existBranch = Branches::find()->where(['branch_name' => $rowData[0][2]])->one();
			if (!$existBranch) {
				$branch = new Branches();
				//$branch->id = $rowData[0];
				$branch->companies_company_id = $rowData[0][1];
				$branch->branch_name = $rowData[0][2];
				$branch->branch_address = $rowData[0][3];
				$branch->branch_created_date = $rowData[0][4];
				$branch->branch_status = $rowData[0][5];
				if ($branch->save()) {
					$successMessage .= 'Branch ' . $branch->branch_name .' has imported.<br>';
				}
			}
			else {
				$errorMessage .= 'Branch with name <b>' . $existBranch->branch_name .'</b> is already exist.<br>';
			}
		}
		if (strlen($successMessage)) {
			Yii::$app->session->setFlash('success', $successMessage);
		}
		if (strlen($errorMessage)) {
			Yii::$app->session->setFlash('error', $errorMessage);
		}

		return $this->render('import');
	}
    /**
     * Displays a single Branches model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Branches model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('create-branch')) {
            $model = new Branches();
            if ($model->load(Yii::$app->request->post())) {
                $model->branch_created_date = date('Y-m-d h:m:s');
				if ($model->save()) {
					echo 1;
				}
				else {
					echo 0;
				}
                //return $this->redirect(['index', 'model' => $model]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }
        }
        else {
            throw new ForbiddenHttpException;
        }

    }

    /**
     * Updates an existing Branches model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->branch_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Branches model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionList($id)
    {
        $countBranches = Branches::find()
            ->where(['companies_company_id' => $id])
            ->count();

        $branches = Branches::find()
            ->where(['companies_company_id' => $id])
            ->all();
        if ($countBranches > 0) {
            foreach ($branches as $branch) {
                echo '<option value="' . $branch->branch_id . '">' . $branch->branch_name . '</option>';
            }
        }
        else {
            echo '<option>-</option>';
        }
    }

    /**
     * Finds the Branches model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Branches the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Branches::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

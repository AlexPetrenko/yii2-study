/**
 * Created by apetrenko on 8/23/15.
 */
$(function(){

    $(document).on('click', '.fc-day', function(){
       var date = $(this).attr('data-date');

        $.get(
          'index.php?r=events/create',
            {
                date : date
            },
            function(data) {
                $('#modal').modal('show')
                    .find('#modalContent')
                    .html(data);
            }
        );
    });


    $('#modalButton').on('click', function(){
       $('#modal').modal('show')
           .find('#modalContent')
           .load($(this).val());
    });
});
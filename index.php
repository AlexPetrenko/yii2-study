<?php

$env = isset($_SERVER['ALMA_ENVIRONMENT']) ? $_SERVER['ALMA_ENVIRONMENT'] : 'live';
defined('YII_ENV') or define('YII_ENV', $env);

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/aliases.php');

switch (YII_ENV) {
	case 'dev':
		defined('YII_DEBUG') or define('YII_DEBUG', true);
		$config = yii\helpers\ArrayHelper::merge(
			require(__DIR__ . '/../../common/config/main.php'),
			require(__DIR__ . '/../../common/config/main-local.php'),
			require(__DIR__ . '/../config/main.php'),
			require(__DIR__ . '/../config/main-local.php')
		);
		break;

	case 'stage':
		defined('YII_DEBUG') or define('YII_DEBUG', true);
		$config = yii\helpers\ArrayHelper::merge(
			require(__DIR__ . '/../../common/config/main.php'),
			require(__DIR__ . '/../../common/config/main-stage.php'),
			require(__DIR__ . '/../config/main.php')
		);
		break;

	case 'live':
	default:
		defined('YII_DEBUG') or define('YII_DEBUG', false);
		$config = yii\helpers\ArrayHelper::merge(
			require(__DIR__ . '/../../common/config/main.php'),
			require(__DIR__ . '/../config/main.php')
		);
		break;
}

$application = new yii\web\Application($config);
$application->run();
